scalaVersion := "3.0.0"

enablePlugins(ScalaJSPlugin)

scalaJSUseMainModuleInitializer := true

libraryDependencies ++= Seq(
  ("com.lihaoyi" %%% "scalatags" % "0.8.5").cross(CrossVersion.for3Use2_13),
  ("org.scala-js" %%% "scalajs-dom" % "1.1.0").cross(CrossVersion.for3Use2_13),
)
