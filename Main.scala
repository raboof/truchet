import scala.util.Random

import scalatags.generic.Bundle

type Transform = String
type Colors = (String, String)

case class Tile(paths: Seq[String], rotation: Int = 0):
  def rotate2 = Seq(this, this.copy(rotation = rotation + 90))
  def rotate4 = Seq(
    this,
    this.copy(rotation = rotation + 90),
    this.copy(rotation = rotation + 180),
    this.copy(rotation = rotation + 270)
  )
  def asSvg[Builder, Output <: FragT, FragT](colors: Colors, bundle: Bundle[Builder, Output, FragT]): bundle.Tag =
    import bundle.svgTags._
    import bundle.svgAttrs._
    import bundle.implicits._
    val (outer, inner) = colors
    g(cx := 0, cy := 0, transform := s"rotate($rotation 150 150)")(
      rect(x := "0", y := "0", width := "300", height := "300", style := outer),
    )(
      paths.map(p => path(d := p, style := inner))
    )(
      Seq(
        circle(cx := "0", cy := "0", r := "100", style := outer),
        circle(cx := "0", cy := "300", r := "100", style := outer),
        circle(cx := "300", cy := "0", r := "100", style := outer),
        circle(cx := "300", cy := "300", r := "100", style := outer),
        circle(cx := "0", cy := "150", r := "50", style := inner),
        circle(cx := "150", cy := "0", r := "50", style := inner),
        circle(cx := "300", cy := "150", r := "50", style := inner),
        circle(cx := "150", cy := "300", r := "50", style := inner)
      )
    )

object Tile:
  def apply(path: String) = new Tile(Seq(path))

// The conversion to SVG for these is based on
// https://cheapbotstootsweet.com/source/?url=https://botsin.space/@Truchet_Nested
// by https://chitter.xyz/@serindelaunay
val frown: Tile = Tile("M 100 0 A 200 200 0 0 0 300 200 L 300 100 A 100 100 0 0 1 200 0 L 100 0 z")
val slash: Tile = Tile(Seq("M 100 0 A 200 200 0 0 0 300 200 L 300 100 A 100 100 0 0 1 200 0 L 100 0 z",  "M 0 100 A 200 200 0 0 1 200 300 L 100 300 A 100 100 0 0 0 0 200 L 0 100 z"))
val star: Tile = Tile(Seq())
val cross: Tile = Tile("M 0 0 L 300 0 L 300 300 L 0 300 L 0 0 z")
val plus: Tile = Tile("M 100 0 L 100 100 L 0 100 L 0 200 L 100 200 L 100 300 L 200 300 L 200 200 L 300 200 L 300 100 L 200 100 L 200 0 z")
val line: Tile = Tile("M 100 0 L 100 300 L 200 300 L 200 0 z")
val tee: Tile = Tile("M 100 0 A 100 100 0 0 1 0 100 L 0 200 L 300 200 L 300 100 A 100 100 0 0 1 200 0 z")
// My additions:
val nose: Tile = Tile("M 100 0 L 100 150 L 200 150 L 200 0 z")

val default: Colors =
  ("fill:white;fill-opacity:1;stroke:none",
   "fill:black;fill-opacity:1;stroke:none")

type TilePicker = Seq[Int] => Tile
type SplitPicker = Seq[Int] => Boolean

/**
 * The 'structure' of the image.
 *
 * Starts with a grid and then 'deepens' quadtree-style. That is unnecessarily
 * restrictive, but yields pleasing patterns.
 */
case class Tree(maxDepth: Int, branches: Seq[Seq[Option[Tree]]]):
  /**
   * Return an updated Tree that, on the currently-lowest level,
   * splits some tiles up quadtree-style.
   *
   * Future idea: we can move the max depth into the splitPicker,
   * then we don't need an explicit 'deepen' anymore and can decide
   * the splits while rendering instead
   */
  def deepen(splitPicker: SplitPicker, p: Seq[Int] = Nil): Tree =
    new Tree(maxDepth + 1,
      branches.zipWithIndex.map((col, x) =>
        col.zipWithIndex.map((node, y) =>
          val p2 = p :+ (y+x*col.length)
          node match
          case Some(tree) => Some(tree.deepen(splitPicker, p2))
          case None =>
            if maxDepth == 1 && splitPicker(p2) then
              Some(Tree(2, 2))
            else
              None
        )))

  /** drop one level of a 2x2 tree*/
  def tail(splitPicker: SplitPicker): Tree =
    new Tree(maxDepth - 1,  Seq(Seq(branches.head.head))).deepen(splitPicker)

  /**
   * Collect tiles and their transformations. Don't 'squash' transformations yet, so we
   * can use them to determine the ordering (most transforms = smallest layer = rendered on top)
   */
  private def renderRec[Builder, Output <: FragT, FragT](p: Seq[Int], currentTransforms: List[String], tilePicker: TilePicker, colors: Colors, bundle: Bundle[Builder, Output, FragT]): Seq[(Seq[String], bundle.Tag)] =
    import bundle.svgTags._
    branches.zipWithIndex.flatMap((col, x) =>
      col.zipWithIndex.flatMap((node, y) =>
          val nextTransforms = currentTransforms :+ s"translate(${x * 300} ${y * 300}) "
          val p2 = p :+ y+x*col.length
          node match
          case Some(tree) =>
            tree.renderRec(p2, nextTransforms :+ "scale(0.5 0.5) ", tilePicker, colors.swap, bundle)
          case None =>
            Seq((nextTransforms, g(tilePicker(p2).asSvg(colors, bundle))))
      ))

  /**
   * Render the tree to SVG
   */
  def render[Builder, Output <: FragT, FragT](tilePicker: TilePicker, bundle: Bundle[Builder, Output, FragT]): scalatags.generic.TypedTag[Builder, Output, FragT] =
    import bundle.svgTags._
    import bundle.svgAttrs._
    import bundle.implicits._

    svg(
      xmlns := "http://www.w3.org/2000/svg",
      viewBox := s"0 0 ${branches.length*300} ${branches(0).length*300}",
//      viewBox := s"-100 -100 ${branches.length*300+200} ${branches(0).length*300+200}",
      g(renderRec(Nil, Nil, tilePicker, default, bundle)
      .sortBy(_._1.length)
      .map((transforms, tile) =>
          g(transform:=transforms.mkString(" "), tile)
        )
      )
    )

object Tree:
  /**
   * Create a tree of depth '1'
   */
  def apply(w: Int, h: Int): Tree =
    Tree(
      1,
      for (x <- 0 to (w-1))
        yield for (y <- 0 to (h-1))
          yield None
        )

import org.scalajs.dom

/**
 * 'purely functional' random hash
 */
def hash(seed: Int, seq: Seq[Int], modulo: Int): Int =
  import java.security.MessageDigest
  import java.math.BigInteger
  return new Random(seq.foldLeft(seed)((acc, i) =>
    (acc+3) * (i+7)
  )).nextInt(modulo)

@main def main() =
  // The tile set is the one from
  // https://christophercarlson.com/portfolio/multi-scale-truchet-patterns/
  val carlson = slash.rotate2 ++ frown.rotate4 ++ line.rotate2 ++ tee.rotate4 :+ star :+ cross :+ plus
  val mine = nose.rotate4
  val all = carlson ++ mine
  // only round tiles
  val round = slash.rotate2 ++ frown.rotate4 ++ line.rotate2 ++ tee.rotate4 :+ star :+ cross

  val tiles = carlson

  val seed = new Random().nextInt()
  def pickAtRandom(path: Seq[Int]) = tiles(hash(seed, path, tiles.length))
  def splitAtRandom(path: Seq[Int]) = hash(seed, path, 5) == 0

  def infiniteOrigin(path: Seq[Int]) = path.sum == 0  || splitAtRandom(path)

  val nodejs = scala.util.Try(dom.window).map(_ => false).getOrElse(true)
  if nodejs then
    val tree = Tree(6, 4)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)

    val image = tree.render(pickAtRandom, scalatags.Text).render
    println(image)
  else
    var tree = Tree(2, 2)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)
      .deepen(infiniteOrigin)

    var image = tree.render(pickAtRandom, scalatags.JsDom)

    val clientWidth = dom.document.body.clientWidth
    var width = clientWidth
    var rendered = image.render
    rendered.setAttribute("style", s"width: ${width}px")
    dom.document.body.appendChild(rendered)
    dom.window.setInterval(() =>
      width = width + 30
      if (width > 2*clientWidth)
        width = width / 2
        tree = tree.tail(infiniteOrigin)
        image = tree.render(pickAtRandom, scalatags.JsDom)
        val old = rendered
        rendered = image.render
        dom.document.body.replaceChild(rendered, old)
      rendered.setAttribute("style", s"width: ${width}px"),
      20)


